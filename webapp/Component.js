sap.ui.define(
	[
		"sap/ui/core/UIComponent",
		"sap/ui/core/Fragment",
		"be/fiddle/pluginManager/util/Formatter"
	],
	function(UIComponent, Fragment, Formatter) {
		"use strict";

		/**
		* @name        be.fiddle.pluginManager.Component
		* @alias       Component
		* @instance
		* @public
		* @class
		* <p></p>
		*/
		const Component = UIComponent.extend(
			"be.fiddle.pluginManager.Component",
			/**@lends be.fiddle.pluginManager.Component.prototype **/ {
				metadata: {
					manifest: "json"
				}
			}
		);

		/**
		 * @method init
		 * @public
		 * @instance
		 * @memberof be.fiddle.pluginManager.Component
		 * <p> </p>
		 * 
		 */
		Component.prototype.init = function(){
			if (UIComponent.prototype.init ) {
				UIComponent.prototype.init.apply(this,arguments);
			}

			this._headerItem = sap.ushell.Container.getRenderer("fiori2").addHeaderEndItem(
				"sap.ushell.ui.shell.ShellHeadItem", 
				{
					icon: "sap-icon://multi-select",
					press: this.onPluginIconPress.bind(this),
					visible: true,
					text: this.getModel("i18n").getResourceBundle().getText("PluginManager.description")
				}, 
				true, 
				false,
				["app","home"] //show the header-item both in apps and in the home screen
			);

			let pluginService = sap.ushell.Container.getService("PluginManager");
			this.getModel("plugins").setData(pluginService.getRegisteredPlugins());
		};	

		/**
		 * @method destroy
		 * @public
		 * @instance
		 * @memberof be.fiddle.pluginManager.Component
		 * <p>object destructor: remove any pending event handlers and taskrepeaters</p>
		 */
		Component.prototype.destroy = function(){
			//close any lingering popup
			if (this._manager ) this._manager.close();

			//remove any shell elements you added
			sap.ushell.Container.getRenderer("fiori2").hideHeaderEndItem([this._headerItem.getId()]);
			this._headerItem.destroy();

			//make sure everything is destroyed properly
			UIComponent.prototype.destroy.apply(this,arguments);
		};

		/**
		 * @method onPluginIconPress
		 * @public
		 * @instance
		 * @memberof be.fiddle.pluginManager.Component
		 * @param {event} event
		 * <p></p>
		 */
		Component.prototype.onPluginIconPress = function(event){
			Fragment.load({
				name:"be.fiddle.pluginManager.fragments.Plugins", 
				controller:this
			}).then(function( dialog ){
				this._manager = dialog;
				dialog.open(); //I seem to recall there being a setting to prevent the dialog from being registered on the core.
				//this.addDependent(dialog);
				dialog.setModel(this.getModel("i18n"), "i18n");
				dialog.setModel(this.getModel("plugins"), "plugins");

				//self destruct
				dialog.attachEvent("afterClose", {}, 
					function(){ 
							this._manager.destroy();
							this._manager = null;
					}, this );
			}.bind(this));
		};

		/**
		 * @method closeManager
		 * @public
		 * @instance
		 * @memberof be.fiddle.pluginManager.Component
		 * @param {event} event
		 * <p></p>
		 */
		Component.prototype.closeManager = function(event ){
			if (this._manager) this._manager.close();
		}
		
		/**
		 * @method togglePlugin
		 * @public
		 * @instance
		 * @memberof be.fiddle.pluginManager.Component
		 * @param {event} event
		 * <p></p>
		 */
		Component.prototype.togglePlugin = function(event){
			let item = event.getParameter("listItem");
			let path = item.getBindingContext("plugins").getPath();
			let name = path.split("/")[path.split("/").length - 1];
			let plugin = item.getBindingContext("plugins").getObject()
			let pluginService = sap.ushell.Container.getService("PluginManager");

			if ( ( plugin.component + ".Component" ) === this.getManifest().name ){
				return;
			}

			if (!item.isSelected() ){
				//here it gets tricky. We need to get a handle on the component, and launch a delayed promise to destroy it.
				//this also means that every plugin MUST implement the destroy functionality and remove any
				//shell items it added.
				//Meanwhile, we also need to change the enabled state of the plugin inside the plugin manager

				//what follows is icky:
				let plugins = sap.ui.core.Component.registry.filter(function(oComp){
					return ( oComp.getManifest().name === ( plugin.component + ".Component") );
				});

				plugins.forEach(function(handle){
					handle.destroy();
				});

			} else {
				//this is even worse. we need to somehow figure out how to register, load, or re-activate a plugin.
				let newPlugin = { };
				newPlugin[name] = {
					"enabled":true,
					"loaded":false,
					"component":plugin.component,
					"url":plugin.url,
					"sap-ushell-plugin-type":plugin["sap-ushell-plugin-type"]
				};
				
				pluginService.registerPlugins( newPlugin );
			}
		};

		return Component;
	}
);
