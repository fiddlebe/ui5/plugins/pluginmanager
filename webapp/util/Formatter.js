sap.ui.define(
	[], 
	function() {
    "use strict";

    /**
     * @name 	be.fiddle.pluginManager.util.Formatter
     * @alias 	be.fiddle.pluginManager.util.Formatter
     * @constructor
     * @public
     * @class
     * this is the formatter for the ui5express app. it contains the different formatting routines that are used in the views.<br/>
     * Add your methods onto the ExpressUI5Formatter constant.
     */
    const Formatter = {};

    /**
     * @method getObjectName
     * @public
     * @instance
     * @memberof be.fiddle.pluginManager.util.Formatter
     * @param {object} treeItem - the full object bound to a treeitem
     * @return {string} the name of the plugin
     * <p></p>
     */
    Formatter.getObjectName = function(treeItem){
      //if the formatter is bound with the full-name, the control gets passed as the this-context
      if (this.hasOwnProperty("sParentAggregationName") && this.getBindingContext ) { 
        let binding = this.getBindingContext("plugins");
        let parts = binding.getPath().split("/");
        return parts[parts.length-1]; //return the last piece of the path
      }

      //if the formatter was defined in the binding as '.Formatter.method', the owner of the view gets passed the this-context.
      //in such cases, we can't get the binding of the ui-element, so we have to resort to a defaulting
      return "Formatter called incorrectly";
    };

    return Formatter;
  }, 
  /*export*/true
);
