# Plugin manager

The plugin to manage your plugins!

![pluginmanager](https://gitlab.com/fiddlebe/ui5/plugins/pluginmanager/uploads/1ff9751562b17c53936add9ce12ab999/pluginmanager.gif)

Disclaimer:

If you want this to work properly, you must make sure that your plugins also correctly implement a destroy method. In that destroy method you need to remove any shell-elements that you added during the init phase. Deregister any event handlers, destory any contained elements, stop any render loops you may have running and finally call the super-destroy().

Otherwise, things will remain lingering in the dom, on the shell, or in background calculations.
